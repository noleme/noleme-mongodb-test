package com.noleme.mongodb.test;

import com.noleme.mongodb.MongoDBClient;
import com.noleme.mongodb.configuration.MongoDBConfiguration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 04/04/2020
 */
public class MockDBClientTest
{
    @Test
    void testMockDBClient()
    {
        Assertions.assertDoesNotThrow(() -> {
            MongoDBClient client = new MockDBClient(new MongoDBConfiguration().set(MongoDBConfiguration.DATABASE, "test"));

            Assertions.assertEquals(0, client.db().getCollection("dummy").count());
        });
    }
}
